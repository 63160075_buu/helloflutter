import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String ItalyGreeting = "Ciao Flutter";
String laosGreeting = "Sabaidee Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? spanishGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.accessibility_sharp)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? ItalyGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.ac_unit)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? laosGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.access_alarm))
          ],
        ),
        body: Center(
            child: Text(
          displayText,
          style: TextStyle(fontSize: 24),
        )),
      ),
    );
  }
}
